<tilemap tileswide="32" tileshigh="32" tilewidth="16" tileheight="16">
  <layer number="2" name="Ground">
    <tile x="0" y="0" tile="1" rot="0" flipX="false"/>
    <tile x="1" y="0" tile="1" rot="0" flipX="false"/>
    <tile x="2" y="0" tile="1" rot="0" flipX="false"/>
    <tile x="3" y="0" tile="1" rot="0" flipX="false"/>
  </layer>
  <layer number="1" name="Collisions">
    <tile x="0" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="1" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="2" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="3" y="0" tile="0" rot="0" flipX="false"/>
  </layer>
  <layer number="0" name="Decorations">
    <tile x="0" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="1" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="2" y="0" tile="0" rot="0" flipX="false"/>
    <tile x="3" y="0" tile="0" rot="0" flipX="false"/>
  </layer>
</tilemap>