import subprocess
import sys
import hashlib
import os
import PIL.Image as Image

def command(command_array):
    return str(subprocess.run(command_array, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, text=True).stdout).strip('\n')


def fix(file_names, xml_file):
    unique = list()
    md5only = list()
    redirection = dict()
    bad_files = list()
    print("Generating MD5")
    for i in file_names:
        md5 = hash_md5(i)
        name = i
        number = int(name.split("_")[len(name.split("_")) - 1].split(".")[0])
        if not md5only.__contains__(md5):
            md5only.append(md5)
            unique.append(number)
        else:
            redirection[number] = unique[md5only.index(md5)]
            bad_files.append(name)
    if len(bad_files) == 0:
        print("All files have different MD5 hash")
    else:
        input_file = open(xml_file, "r")
        output_file = open("Output.xml", "w")
        print("Refactoring .XML")
        for i in input_file:
            line = i.strip("\n")
            index = line.find("tile=")
            if index == -1:
                output_file.write(line + "\n")
            else:
                a = line.find("tile=") + len("tile=\"")
                b = a + line[a:].find("\"")
                number = int(line[a:b])
                if redirection.__contains__(number):
                    output_file.write(line[:a] + str(redirection[number]) + line[b:] + "\n")
                else:
                    output_file.write(line[:a] + str(number) + line[b:] + "\n")
        input_file.close()
        output_file.close()
        print("Deleting duplicates")
        for i in bad_files:
            os.remove(i)


def main():
    if len(sys.argv) - 1 == 0:
        print("This program NEEDS TO HAVE 2 arguments")
        print("argument 1: .png format")
        print("argument 2: related .xml")
        print("Example:\n" + sys.argv[0] + " A_ B.xml")
        print("Same .png files with A_ prefix will be deleted (A_01.png)")
        print("If .xml had any deleted indexes, they will be changed to corresponding duplicates origin")
    elif len(sys.argv) - 1 == 2:
        template = sys.argv[1]
        xml_file = sys.argv[2]
        print("Template: " + template)
        print("XML file: " + xml_file)
        file_names = command(["ls", template + "*.png"]).strip("\n").split("\n")
        if len(file_names) == 1:
            if file_names[0] == "":
                print("No .png files by " + template + " pattern")
            else:
                print("One file does not need merging")
        else:
            fix(file_names, xml_file)
    else:
        print("Try \"" + sys.argv[0] + "\" command")


def hash_md5(file_name):
    file = open(file_name, 'rb')
    data = file.read()
    md5 = hashlib.md5()
    md5.update(data)
    return md5.hexdigest()


main()
