import PIL.Image as Image
import os
import sys


def main(file_name, template):
    i = Image.open(file_name)
    (width, height) = i.size
    width = int(width / 16)
    height = int(height / 16)
    length = len(str(height * width - 1))
    index = 0
    for y in range(height):
        for x in range(width):
            cropped = i.crop((x*16, y*16, x*16+16, y*16+16))
            cropped.save(("./Result/" + template + "{:0="+str(length)+"}.png").format(index), "PNG")
            index += 1


main(sys.argv[1], sys.argv[2])
