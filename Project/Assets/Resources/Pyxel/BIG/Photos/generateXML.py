import sys


def format_tile(x, y, tile):
    return "\n    <tile x=\""+str(x)+"\" y=\""+str(y)+"\" tile=\""+str(tile)+"\" rot=\"0\" flipX=\"false\"/>"


def format_xml(output_path, width, height, layer_count):
    output_file = open(output_path, 'w')
    output_file.write("<tilemap tileswide="+str(width)+" tileshigh="+str(height)+" tilewidth=\"16\" tileheight=\"16\">")
    index = 0
    for layer in range(layer_count):
        layer_number = layer_count - layer - 1
        output_file.write("\n  <layer number=\""+str(layer_number)+"\" name=\"Name_"+str(layer_number)+"\">")
        for y in range(height):
            for x in range(width):
                output_file.write(format_tile(x, y, index))
                index += 1
        output_file.write("\n  </layer>")
    output_file.write("\n</tilemap>")


if len(sys.argv) == 5:
    main(sys.argv[4], int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
else:
    print("Need 4 arguments")
    print("structure: " + sys.argv[0] + " width height layer_count file_name")
