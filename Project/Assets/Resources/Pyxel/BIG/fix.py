import os
import sys
import PIL.Image as Image
import hashlib


def extension(file_name):
    return file_name[file_name.rfind(".") + 1:].upper()


def argument_test():
    flag = False
    argument_count = len(sys.argv)
    if argument_count == 1:
        print("{0} command structure".format(sys.argv[0]))
        print("{0} xml_name tile_template [layer_files]".format(sys.argv[0]))
        print("example:")
        print("{0} Map Tile Layer.png Layer_Two.png".format(sys.argv[0]))
        return False
    elif argument_count < 4:
        print("Not enough arguments")
        print("Inspect \"{0}\" for example and explanation".format(sys.argv[0]))
        return False
    for i in range(len(sys.argv) - 3):
        argument = sys.argv[i + 3]
        exists = os.path.isfile(argument)
        if not exists:
            print("{:<12} <- File doesn't exists".format(argument))
            flag = True
        elif not extension(argument) == "PNG":
            print("{:<12} <- Not .png file".format(argument))
            flag = True
    if flag:
        return False
    return True


def create_folder():
    index = 1
    while os.path.isdir("Result_{}".format(index)):
        index += 1
    name = "Result_{}".format(index)
    os.mkdir(name)
    print("Results will be saved in folder: {}".format(name))
    return name


def image_test(image_list):
    (width, height) = Image.open(image_list[0]).size
    if width % 16 != 0 or height % 16 != 0:
        if width % 16 != 0 and height % 16 != 0:
            print("Width and Height is NOT dividable by 16 (Tile size)")
        elif width % 16 != 0:
            print("Width is NOT dividable by 16 (Tile width)")
        else:
            print("Height is NOT dividable by 16 (Tile height)")
        return False
    for file in image_list:
        (x, y) = Image.open(file).size
        if x != width or y != height:
            print("Layer files must be same size")
            return False
    return True


def format_xml(output_path, width, height, layer_count):
    def format_tile(tile_x, tile_y, tile):
        return "\n    <tile x=\"{0}\" y=\"{1}\" tile=\"{2}\" rot=\"0\" flipX=\"false\"/>".format(tile_x, tile_y, tile)
    output_file = open(output_path, 'w')
    s = "<tilemap tileswide=\"{0}\" tileshigh=\"{1}\" tilewidth=\"16\" tileheight=\"16\">".format(width, height)
    output_file.write(s)
    index = 0
    for layer in range(layer_count):
        layer_number = layer_count - layer - 1
        output_file.write("\n  <layer number=\""+str(layer_number)+"\" name=\"Name_"+str(layer_number)+"\">")
        for y in range(height):
            for x in range(width):
                output_file.write(format_tile(x, y, index))
                index += 1
        output_file.write("\n  </layer>")
    output_file.write("\n</tilemap>")


def image_split(file_name, result_folder, template, length, index, tile_list):
    i = Image.open(file_name)
    (width, height) = i.size
    width = int(width / 16)
    height = int(height / 16)
    for y in range(height):
        for x in range(width):
            cropped = i.crop((x*16, y*16, x*16+16, y*16+16))
            name = ("./{0}/{1}{2:0=" + str(length) + "}.png").format(result_folder, template, index)
            tile_list.append(name)
            cropped.save(name, "PNG")
            index += 1
    return index, tile_list


def merge(image_list, result_folder, xml_file):
    def hash_md5(file_name):
        file = open(file_name, 'rb')
        data = file.read()
        md5 = hashlib.md5()
        md5.update(data)
        return md5.hexdigest()

    def parse_xml_number(s):
        a = s.find("tile=\"") + len("tile=\"")
        b = a + s[a:].find("\"")
        return int(s[a:b])

    def parse_file_number(s):
        a = s.rfind("_") + 1
        b = s.rfind(".")
        return int(s[a:b])

    def parse_xml_tile(s, index):
        a = s.find("tile=\"") + len("tile=\"")
        b = a + s[a:].find("\"")
        return s[:a] + str(index) + s[b:]
    md5s = dict()
    redirect = dict()
    bad_files = list()
    print("Generating MD5")
    for image in image_list:
        md5_hash = hash_md5(image)
        if not md5s.__contains__(md5_hash):
            md5s[md5_hash] = parse_file_number(image)
        else:
            bad_files.append(image)
            bad_index = parse_file_number(image)
            good_index = md5s[md5_hash]
            redirect[bad_index] = good_index
    print("Refactoring .XML")
    input_file = open(result_folder + xml_file, "r")
    output_name = result_folder + "X.txt"
    if result_folder + xml_file == output_name:
        output_name = result_folder + "Y.txt"
    output_file = open(output_name, "w")
    for i in input_file:
        line = i.strip("\n")
        if line[0] == "<":
            if line[1] == "/":
                output_file.write("\n")
            output_file.write(line)
        elif line[2] == "<":
            output_file.write("\n" + line)
        else:
            index = parse_xml_number(line)
            if not redirect.__contains__(index):
                output_file.write("\n" + line)
            else:
                output_file.write("\n" + parse_xml_tile(line, redirect[index]))
    input_file.close()
    output_file.close()
    os.remove(result_folder + xml_file)
    os.rename(output_name, result_folder + xml_file)
    print("Deleting duplicates")
    for file in bad_files:
        os.remove(file)


def main():
    os.system("clear")
    flag = argument_test()
    if not flag:
        print("ARGUMENTS FAILED STRUCTURE TEST")
        return
    result_folder = create_folder()
    print("ARGUMENTS PASSED STRUCTURE TEST")
    image_list = list()
    for i in range(len(sys.argv) - 3):
        image_list.append(sys.argv[i + 3])
    flag = image_test(image_list)
    if not flag:
        print("IMAGE TEST FAILED")
        return
    print("IMAGE TEST PASSED")
    (width, height) = Image.open(image_list[0]).size
    width = int(width / 16)
    height = int(height / 16)
    print("Generating .XML")
    format_xml(result_folder + "/XML.xml", width, height, len(image_list))
    index = 0
    # template = sys.argv[2] + "_"
    template = "T_"
    print("Splitting images")
    length = len(str(width * height * len(image_list) - 1))
    tile_list = list()
    for image_file in image_list:
        (index, tile_list) = image_split(image_file, result_folder, template, length, index, tile_list)
    merge(tile_list, result_folder + "/", "XML.xml")


main()
