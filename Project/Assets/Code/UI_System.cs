using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_System : MonoBehaviour
{
    private Dictionary<string, Text> UI_Text;
    public UI_System(GameObject canvas)
    {
        UI_Text = new Dictionary<string, Text>();
        if(canvas && canvas.transform.childCount > 0)
        {
            for (int i = 0; i < canvas.transform.childCount; i++)
            {
                GameObject a = canvas.transform.GetChild(i).gameObject;
                if (a.name == "Statistic")
                {
                    Text name = a.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
                    Text value = a.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
                    this.UI_Text[name.text] = value;
                }
            }
        }

    }
    public string Get(string key) => UI_Text[key].text;
    public void Set(string key, string value) => UI_Text[key].text = value;
}