using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int x = 0;
    public int y = 0;
    private Vector3 target;
    public float cameraOffset_x = 0;
    public void Start()
    {
        target = new Vector3(x, y, 0);
    }
    void Update()
    {
        if (!Core.ControlAccess)
            return;
        int horizontal = 0;
        int vertical = 0;

        if (Input.GetKeyDown(KeyCode.W)) vertical++;
        if (Input.GetKeyDown(KeyCode.A)) horizontal--;
        if (Input.GetKeyDown(KeyCode.S)) vertical--;
        if (Input.GetKeyDown(KeyCode.D)) horizontal++;

        if (horizontal != 0 || vertical != 0)
        {
            if (Core.map.CanGoTo(x + horizontal, y + vertical))
            {
                x += horizontal;
                y += vertical;
                Core.map.MoveFromTo(x - horizontal, y - vertical, x, y, EntityType.Player);
                Core.triggers.Invoke(x, y);
                Core.entities.ExecuteAll();
            }
            else if(Core.map.Get(x + horizontal, y + vertical) == (int)EntityType.NPC)
            {
                Core.entities.entities[x + horizontal, y + vertical].TakeDamage(Core.Damage);
                Core.entities.ExecuteAll();
            }
            //this.transform.position = new Vector3(x, y, Core.EntityHeight);
            //Camera.main.transform.position = new Vector3(x, y, Camera.main.transform.position.z);
        }
        target = new Vector3(Mathf.Lerp(target.x, x, 0.1f), Mathf.Lerp(target.y, y, 0.1f), Core.EntityHeight + y * 0.001f);
        Camera.main.transform.position = new Vector3(target.x + cameraOffset_x, target.y, Camera.main.transform.position.z);
        this.transform.position = target;
        if(Core.entities != null)
            Core.entities.UpdateVisuals();
    }
    public void ChangePosition(int x, int y)
    {
        this.x = x;
        this.y = y;
        target = new Vector3(x, y, target.z);
        Camera.main.transform.position = new Vector3(x, y, Camera.main.transform.position.z);
        this.transform.position = target;
    }
}
