using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Map_System
{
    public delegate void ObjectTrigger(GameObject o);
    Sprite[] sprites;
    List<int[,]> layers;
    public GameObject map;

    int layerCount;
    public int width;
    public int height;

    /// <summary>
    /// Values:
    /// -1 inaccesible
    ///  0 accesible
    ///  1 player
    ///  2 NPC
    /// </summary>
    int[,] pathway;

    public Tuple<int, int> PlayerLocation()
    {
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                if (pathway[x, y] == 1)
                    return new Tuple<int, int>(x, y);
            }
        }
        return new Tuple<int, int>(-1, -1);
    }
    public void GeneratePathway()
    {
        pathway = new int[width, height];
        for(int y = 0; y < height; y++) 
            for(int x = 0; x < width; x++) 
                pathway[x, y] = -1;
    }
    public void GeneratePathway(int layerIndex, int acceptenceIndex)
    {
        int[,] layer = layers[layerIndex];
        pathway = new int[width, height];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                pathway[x, height - y - 1] = layer[x, y] == acceptenceIndex ? 0 : -1;
    }
    public void GeneratePathway(int[,] layer, int acceptenceIndex)
    {
        pathway = new int[width, height];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                pathway[x, height - y - 1] = layer[x, y] == acceptenceIndex ? 0 : -1;
    }
    public bool CanGoTo(int x, int y)
    {
        if (x >= width || x < 0 || y >= height || y < 0)
            return false;
        return pathway[x, y] == 0;
    }
    public void MoveFromTo(int fromX, int fromY, int toX, int toY, EntityType type)
    {
        pathway[fromX, fromY] = 0;
        pathway[toX, toY] = type == EntityType.Player ? 1 : 2;
    }
    public void MoveFromTo(int fromX, int fromY, int toX, int toY)
    {
        if(fromX != toX || fromY != toY)
        {
            pathway[toX, toY] = pathway[fromX, fromY];
            pathway[fromX, fromY] = 0;
            //Core.entities.Move(fromX, fromY, toX, toY);
        }
    }
    public void Set(int x, int y, int v)
    {
        if (x >= 0 && x < width && y >= 0 && y < height) pathway[x, y] = v;
    }
    public int Get(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height ? pathway[x, y] : -1;
    }
    public void Load(string folderName)
    {
        Tuple<Sprite[], List<int[,]>> tuple = XML.LoadMap(folderName);
        sprites = tuple.Item1;
        layers = tuple.Item2;
        if(map)
        {
            GameObject.Destroy(map);
        }
        map = new GameObject("Map");
        layerCount = layers.Count;
        for (int i = 1; i < layerCount; i++)
        {
            int[,] layer = layers[i];
            GameObject parent = new GameObject("Layer " + i);
            parent.transform.SetParent(map.transform);
            width = layer.GetLength(0);
            height = layer.GetLength(1);
            int z = i == 1 ? 0 : - 10 - i;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    GameObject t = new GameObject(string.Format("Tile {0}:{1}", x, y), typeof(SpriteRenderer));
                    t.transform.SetParent(parent.transform);
                    t.transform.position = new Vector3(x, height - y - 1, 0);
                    t.GetComponent<SpriteRenderer>().sprite = sprites[layer[x, y]];
                }
            }
            parent.transform.position = new Vector3(parent.transform.position.x, parent.transform.position.y, -i);
        }
    }
}
public enum EntityType
{
    Player = 1,
    NPC = 2
}
