using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data
{
    public static void Reset()
    {
        Core.log.Clear();
        Level_City.Clear();
        Level_Room.Clear();
        Level_Shop.Clear();
        Level_Dungeon_1.Clear();
        Level_Dungeon_2.Clear();
        Level_Dungeon_3.Clear();
    }
    public static class Default
    {
        public static void Load_Map()
        {
            Core.map = Level_City.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(25, 29);
            Core.map.Set(25, 29, (int)EntityType.Player);
            Core.entities = Level_City.entitySystem;
            Core.triggers = Level_City.triggerSystem;
            Core.map.map.SetActive(true);
        }
    }
    public static class Level_City
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if (trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if(entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                    entity_system.Add(new Chest(7, 35, Resources.Load<Sprite>("Sprites\\16x16\\Chest")));
                    entity_system.Add(new Rabbit(10, 10, Resources.Load<Sprite>("Sprites\\16x16\\Rabbit")));
                    entity_system.Add(new Rabbit(20, 10, Resources.Load<Sprite>("Sprites\\16x16\\Rabbit")));
                    entity_system.Add(new Rabbit(30, 10, Resources.Load<Sprite>("Sprites\\16x16\\Rabbit")));
                    entity_system.Add(new Rock(33, 28, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if(map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\City");
                    map_system.GeneratePathway(0, 0);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system = null;
        public static Trigger_System triggerSystem
        {
            get
            {
                if(trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(20, 28, delegate
                    {
                        // From City To Room
                        Core.map.Set(20, 28, 0);
                        Data.Level_City.To_Room();
                        Core.log.WriteLine("Entering Room");
                    });
                    trigger_system.SetTrigger(29, 30, delegate
                    {
                        // From City To Shop
                        Core.map.Set(29, 30, 0);
                        Data.Level_City.To_Shop_1();
                        Core.log.WriteLine("Entering Shop");
                    });
                    trigger_system.SetTrigger(30, 30, delegate
                    {
                        // From City To Shop
                        Core.map.Set(30, 30, 0);
                        Data.Level_City.To_Shop_2();
                        Core.log.WriteLine("Entering Shop");
                    });
                    trigger_system.SetTrigger(20, 16, delegate
                    {
                        // From City To Dungeon_1
                        Core.map.Set(20, 16, 0);
                        Data.Level_City.To_Dungeon_1();
                        Core.log.WriteLine("Entering Dungeon");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_Room()
        {
            Core.map = Level_Room.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(2, 0);
            Core.map.Set(2, 0, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Room.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Room.triggerSystem;
        }
        public static void To_Shop_1()
        {
            Core.map = Level_Shop.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(2, 0);
            Core.map.Set(2, 0, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Shop.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Shop.triggerSystem;
        }
        public static void To_Shop_2()
        {
            Core.map = Level_Shop.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(3, 0);
            Core.map.Set(3, 0, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Shop.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Shop.triggerSystem;
        }
        public static void To_Dungeon_1()
        {
            Core.map = Level_Dungeon_1.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(36, 35);
            Core.map.Set(36, 35, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Dungeon_1.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Dungeon_1.triggerSystem;
        }
    }
    public static class Level_Room
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if(trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if (entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if (map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\Room");
                    map_system.GeneratePathway(0, 0);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system;
        public static Trigger_System triggerSystem
        {
            get
            {
                if (trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(2, 0, delegate
                    {
                        // From City To Room
                        Core.map.Set(2, 0, 0);
                        Data.Level_Room.To_City();
                        Core.log.WriteLine("Entering City");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_City()
        {
            Core.map = Level_City.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(20, 27);
            Core.map.Set(20, 27, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_City.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_City.triggerSystem;
        }
    }
    public static class Level_Shop
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if (trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if (entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if (map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\Shop");
                    map_system.GeneratePathway(0, 31);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system;
        public static Trigger_System triggerSystem
        {
            get
            {
                if (trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(2, 0, delegate
                    {
                        // From Shop To City
                        Core.map.Set(2, 0, 0);
                        Data.Level_Shop.To_City_1();
                        Core.log.WriteLine("Entering City");
                    });
                    trigger_system.SetTrigger(3, 0, delegate
                    {
                        // From Shop To City
                        Core.map.Set(3, 0, 0);
                        Data.Level_Shop.To_City_2();
                        Core.log.WriteLine("Entering City");
                    });
                    trigger_system.SetTrigger(5, 3, delegate
                    {
                        // Close UI
                        if (Core.Shop.activeSelf)
                        {
                            Core.player.GetComponent<Player>().cameraOffset_x = 0;
                            Core.Shop.SetActive(false);
                            Core.log.WriteLine("Closing Shop");
                        }
                    });
                    trigger_system.SetTrigger(6, 2, delegate
                    {
                        // Close UI
                        if (Core.Shop.activeSelf)
                        {
                            Core.player.GetComponent<Player>().cameraOffset_x = 0;
                            Core.Shop.SetActive(false);
                            Core.log.WriteLine("Closing Shop");
                        }
                    });
                    trigger_system.SetTrigger(7, 3, delegate
                    {
                        // Close UI
                        if (Core.Shop.activeSelf)
                        {
                            Core.player.GetComponent<Player>().cameraOffset_x = 0;
                            Core.Shop.SetActive(false);
                            Core.log.WriteLine("Closing Shop");
                        }
                    });
                    trigger_system.SetTrigger(6, 3, delegate
                    {
                        // OPEN UI
                        Core.player.GetComponent<Player>().cameraOffset_x = 5;
                        Core.Shop.SetActive(true);
                        Core.log.WriteLine("Opening Shop");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_City_1()
        {
            Core.map = Level_City.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(29, 29);
            Core.map.Set(29, 29, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_City.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_City.triggerSystem;
        }
        public static void To_City_2()
        {
            Core.map = Level_City.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(30, 29);
            Core.map.Set(30, 29, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_City.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_City.triggerSystem;
        }
    }
    public static class Level_Dungeon_1
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if (trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if (entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                    // Rats
                    entity_system.Add(new Rat(22, 29, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(44, 27, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(56, 38, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(64, 36, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(59, 35, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(63, 33, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(54, 14, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(62, 16, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(50, 8, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(51, 3, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(34, 7, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(18, 5, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(15, 4, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(3, 37, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(4, 35, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(3, 33, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(17, 21, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(19, 19, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(21, 21, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(23, 19, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    entity_system.Add(new Rat(25, 21, Resources.Load<Sprite>("Sprites\\16x16\\Rat")));
                    //Crabs
                    entity_system.Add(new Crab(28, 29, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(29, 27, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(46, 24, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(50, 25, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(6, 8, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(1, 12, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(4, 19, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                    entity_system.Add(new Crab(1, 25, Resources.Load<Sprite>("Sprites\\16x16\\Crab")));
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if (map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\Dungeon_1");
                    map_system.GeneratePathway(0, 190);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system;
        public static Trigger_System triggerSystem
        {
            get
            {
                if (trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(35, 36, delegate
                    {
                        // From Dungeon to City
                        Core.map.Set(35, 36, 0);
                        Data.Level_Dungeon_1.To_City();
                        Core.log.WriteLine("Entering City");
                    });
                    trigger_system.SetTrigger(36, 36, delegate
                    {
                        // From Dungeon to City
                        Core.map.Set(36, 36, 0);
                        Data.Level_Dungeon_1.To_City();
                        Core.log.WriteLine("Entering City");
                    });
                    trigger_system.SetTrigger(17, 7, delegate
                    {
                        // From Dungeon to Dungeon 2
                        Core.map.Set(17, 7, 0);
                        Data.Level_Dungeon_1.To_Dungeon_2();
                        Core.log.WriteLine("Entering Level 2");
                    });
                    trigger_system.SetTrigger(18, 7, delegate
                    {
                        // From Dungeon to Dungeon 2
                        Core.map.Set(18, 7, 0);
                        Data.Level_Dungeon_1.To_Dungeon_2();
                        Core.log.WriteLine("Entering Level 2");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_City()
        {
            Core.map = Level_City.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(20, 15);
            Core.map.Set(20, 15, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_City.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_City.triggerSystem;
        }
        public static void To_Dungeon_2()
        {
            Core.map = Level_Dungeon_2.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(27, 50);
            Core.map.Set(27, 50, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Dungeon_2.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Dungeon_2.triggerSystem;
        }
    }
    public static class Level_Dungeon_2
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if (trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if (entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                    entity_system.Add(new Rock(14, 11, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Rock(14, 5, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Rock(16, 4, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Rock(16, 12, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Rock(18, 11, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Rock(19, 9, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    //entity_system.Add(new Rock(,, Resources.Load<Sprite>("Sprites\\16x16\\Rock")));
                    entity_system.Add(new Skeleton(42, 34, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(42, 42, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(45, 37, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(35, 37, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(21, 37, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(9, 30, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(19, 30, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(17, 22, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(26, 24, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new Skeleton(32, 24, Resources.Load<Sprite>("Sprites\\16x16\\Skeleton")));
                    entity_system.Add(new ArmoredSkeleton(28, 21, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(30, 21, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(27, 11, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(30, 6, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(24, 8, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(41, 8, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                    entity_system.Add(new ArmoredSkeleton(46, 13, Resources.Load<Sprite>("Sprites\\16x16\\ArmoredSkeleton")));
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if (map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\Dungeon_2");
                    map_system.GeneratePathway(0, 239);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system;
        public static Trigger_System triggerSystem
        {
            get
            {
                if (trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(13, 8, delegate
                    {
                        // From City To Room
                        Core.map.Set(13, 8, 0);
                        Data.Level_Dungeon_2.To_Dungeon_3();
                        Core.log.WriteLine("Entering Level 3");
                    });
                    trigger_system.SetTrigger(13, 9, delegate
                    {
                        // From City To Room
                        Core.map.Set(13, 9, 0);
                        Data.Level_Dungeon_2.To_Dungeon_3();
                        Core.log.WriteLine("Entering Level 3");
                    });
                    trigger_system.SetTrigger(13, 7, delegate
                    {
                        // From City To Room
                        Core.map.Set(13, 7, 0);
                        Data.Level_Dungeon_2.To_Dungeon_3();
                        Core.log.WriteLine("Entering Level 3");
                    });
                    trigger_system.SetTrigger(27, 51, delegate
                    {
                        // From City To Room
                        Core.map.Set(27, 51, 0);
                        Data.Level_Dungeon_2.To_Dungeon_1();
                        Core.log.WriteLine("Entering Level 1");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_Dungeon_1()
        {
            Core.map = Level_Dungeon_1.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(18, 6);
            Core.map.Set(18, 6, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Dungeon_1.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Dungeon_1.triggerSystem;
        }
        public static void To_Dungeon_3()
        {
            Core.map = Level_Dungeon_3.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(9, 45);
            Core.map.Set(9, 45, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Dungeon_3.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Dungeon_3.triggerSystem;
        }
    }
    public static class Level_Dungeon_3
    {
        public static void Clear()
        {
            if (entitySystem != null)
            {
                entity_system.DestroyAll();
                entity_system = null;
            }
            if (map_system != null)
            {
                GameObject.Destroy(map_system.map);
                map_system = null;
            }
            if (trigger_system != null)
                trigger_system = null;
        }
        private static Entity_System entity_system = null;
        public static Entity_System entitySystem
        {
            get
            {
                if (entity_system == null)
                {
                    entity_system = new Entity_System(mapSystem.width, mapSystem.height);
                    entity_system.Add(new Bug(21, 31, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(25, 28, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(25, 22, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(33, 27, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Mummy(31, 33, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(38, 27, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(34, 21, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Bug(43, 15, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(53, 18, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Mummy(58, 18, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(58, 12, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Bug(53, 11, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Mummy(45, 7, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(40, 7, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Bug(30, 10, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(21, 10, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(15, 10, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(15, 15, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Mummy(20, 15, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(14, 23, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Mummy(33, 41, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Bug(41, 41, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(49, 44, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(52, 41, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(55, 37, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Bug(49, 34, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                    entity_system.Add(new Mummy(52, 25, Resources.Load<Sprite>("Sprites\\16x16\\Mummy")));
                    entity_system.Add(new Bug(46, 25, Resources.Load<Sprite>("Sprites\\16x16\\Bug")));
                }
                return entity_system;
            }
            set
            {
                entity_system = value;
            }
        }
        private static Map_System map_system = null;
        public static Map_System mapSystem
        {
            get
            {
                if (map_system == null)
                {
                    map_system = new Map_System();
                    map_system.Load("Maps\\Dungeon_3");
                    map_system.GeneratePathway(0, 380);
                }
                return map_system;
            }
            set
            {
                map_system = value;
            }
        }
        private static Trigger_System trigger_system;
        public static Trigger_System triggerSystem
        {
            get
            {
                if (trigger_system == null)
                {
                    trigger_system = new Trigger_System();
                    trigger_system.SetTrigger(8, 46, delegate
                    {
                        // From 3 to 1
                        Core.map.Set(8, 46, 0);
                        Data.Level_Dungeon_3.To_Dungeon_2();
                        Core.log.WriteLine("Entering Level 2");
                    });
                    trigger_system.SetTrigger(9, 46, delegate
                    {
                        // From 3 to 1
                        Core.map.Set(9, 46, 0);
                        Data.Level_Dungeon_3.To_Dungeon_2();
                        Core.log.WriteLine("Entering Level 2");
                    });
                    trigger_system.SetTrigger(36, 9, delegate
                    {
                        // WIN
                        Core.map.Set(9, 46, 0);
                        Core.UI.SetActive(false);
                        Core.WinScreen.SetActive(true);
                        Core.ControlAccess = false;
                        Core.log.WriteLine("You won Congrats");
                    });
                }
                return trigger_system;
            }
            set
            {
                trigger_system = value;
            }
        }
        public static void To_Dungeon_2()
        {
            Core.map = Level_Dungeon_2.mapSystem;
            Core.player.GetComponent<Player>().ChangePosition(14, 8);
            Core.map.Set(14, 8, (int)EntityType.Player);
            Core.entities.parent.SetActive(false);
            Core.entities = Level_Dungeon_2.entitySystem;
            Core.entities.parent.SetActive(true);
            Core.triggers = Level_Dungeon_2.triggerSystem;
        }
    }
}
