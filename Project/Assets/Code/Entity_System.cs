using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Entity_System
{
    public GameObject parent;
    public Entity[,] entities;
    int width;
    int height;
    public Entity_System(int x, int y)
    {
        parent = new GameObject("Entities");
        entities = new Entity[x, y];
        width = x;
        height = y;
    }
    public void Move(int fromX, int fromY, int toX, int toY)
    {
        if (fromX != toX || fromY != toY)
        {
            entities[toX, toY] = entities[fromX, fromY];
            entities[fromX, fromY] = null;
        }
    }
    public void Add(Entity o)
    {
        int x = o.x;
        int y = o.y;
        if (x >= 0 && x < width &&
            y >= 0 && y < height &&
            entities[x, y] == null)
        {
            o.o.transform.SetParent(parent.transform);
            entities[x, y] = o;
        }
    }
    public void Remove(int x, int y)
    {
        if (x >= 0 && x < width &&
            y >= 0 && y < height &&
            entities[x, y] == null)
        {
            Entity o = entities[x, y];
            entities[x, y] = null;
            GameObject.Destroy(parent.transform.Find(o.o.name).gameObject);
        }
    }
    public void Remove(string name)
    {
        Transform o = parent.transform.Find(name);
        if(o != null)
            GameObject.Destroy(o.gameObject);
    }
    public void Hide()
    {
        parent.SetActive(false);
    }
    public void Show()
    {
        parent.SetActive(true);
    }
    public void LoadAll()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (entities[x, y] != null)
                    entities[x, y].Load();
            }
        }
    }
    public void UpdateVisuals()
    {
        List<Entity> temporary = new List<Entity>();
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (entities[x, y] != null)
                {
                    temporary.Add(entities[x, y]);
                }
            }
        }
        temporary.ForEach(a => a.VisualUpdate());
    }
    public void ExecuteAll()
    {
        List<Entity> temporary = new List<Entity>();
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                if (entities[x, y] != null)
                {
                    temporary.Add(entities[x, y]);
                }
            }
        }
        temporary.ForEach(a => 
        { 
            if(!a.Active)
            {
                if(a.GetType() != typeof(Chest))
                    Core.log.WriteLine(a.o.name + " Died");
                entities[a.x, a.y] = null;
                Core.map.Set(a.x, a.y, 0);
                GameObject.Destroy(a.o);
            }
        });
        temporary.ForEach(a => a.Execute());
    }
    public void DestroyAll()
    {
        List<Entity> temporary = new List<Entity>();
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (entities[x, y] != null)
                {
                    temporary.Add(entities[x, y]);
                }
            }
        }
        temporary.ForEach(a => GameObject.Destroy(a.o));
        GameObject.Destroy(parent);
    }
}
