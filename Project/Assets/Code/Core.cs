using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class Core : MonoBehaviour
{
    public GameObject _ui;
    public GameObject _shop;
    public GameObject _menu;
    public GameObject _gameOver;
    public GameObject _win_screen;
    public GameObject _shop_obj;

    public GameObject _startPlayer;
    public GameObject _startStatisticCanvas;
    public Text _logTextObject;

    private static int entityIndex = 0;
    public static int EntityIndex
    {
        get
        {
            return entityIndex++;
        }
    }

    public static float EntityHeight = -2.5f;
    public static GameObject player;
    public static GameObject statistic_canvas;
    private static Map_System Map = null;
    public static Map_System map
    {
        get
        {
            return Map;
        }
        set
        {
            if(Map != null)
                Map.map.SetActive(false);
            Map = value;
            if (Map != null)
                Map.map.SetActive(true);
        }
    }
    public static UI_System statistics;
    public static Log log;
    public static Trigger_System triggers;
    public static Entity_System entities;
    public static GameObject UI;
    public static GameObject Shop;
    public static GameObject Menu;
    public static GameObject GameOver;
    public static GameObject WinScreen;
    public static Shop shop;

    public static int dmg;
    public static int Damage
    {
        get
        {
            return UnityEngine.Random.Range(dmg - 1, dmg + 1);
        }
        set
        {
            dmg = value;
            statistics.Set("Damage", string.Format("{0}-{1}", dmg - 1, dmg + 1));
        }
    }
        

    private static int hp;
    public static int Health {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
            statistics.Set("Health", hp.ToString());
            if(hp <= 0)
            {
                UI.SetActive(false);
                GameOver.SetActive(true);
                ControlAccess = false;
            }
        }
    }
    private static int df;
    public static int Defence
    {
        get
        {
            return df;
        }
        set
        {
            df = value;
            statistics.Set("Armour", df.ToString());
        }
    }
    private static int gp;
    public static int Money
    {
        get
        {
            return gp;
        }
        set
        {
            gp = value;
            statistics.Set("Money", gp.ToString());
        }
    }
    public static class ShopUtilities
    {
        public static void IncreaseDamage()
        {
            dmg++;
            Damage = dmg;
        }
    }
    public static void ExecuteEntities()
    {
        entities.ExecuteAll();
    }
    public static void ChangeMap(string folderName, int pathLayer, int pathIndex)
    {
        map.Load(folderName);
        map.GeneratePathway(pathLayer, pathIndex);
    }
    public static bool ControlAccess = false;
    void Start()
    {
        UI = _ui;
        Shop = _shop;
        Menu = _menu;
        GameOver = _gameOver;
        WinScreen = _win_screen;
        shop = new Shop(_shop_obj);

        UI.SetActive(false);
        Shop.SetActive(false);
        Menu.SetActive(true);
        GameOver.SetActive(false);
        WinScreen.SetActive(false);

        Menu.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate 
        {
            ControlAccess = true;
            UI.SetActive(true);
            Menu.SetActive(false);
            Data.Default.Load_Map();

            Health = 25;
            Defence = 1;
            Damage = 4;
            Money = 0;
        });
        Menu.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate 
        {
            Application.Quit();
        });
        GameOver.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate
        {
            ControlAccess = true;
            GameOver.SetActive(false);
            UI.SetActive(true);
            Menu.SetActive(false);

            Data.Reset();
            Data.Default.Load_Map();
            entityIndex = 0;

            shop.Reset();

            Health = 25;
            Defence = 1;
            Damage = 4;
            Money = 0;
        });
        WinScreen.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate
        {
            Application.Quit();
        });
        WinScreen.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate
        {
            ControlAccess = true;
            WinScreen.SetActive(false);
            UI.SetActive(true);
            Menu.SetActive(false);

            Data.Reset();
            Data.Default.Load_Map();
            entityIndex = 0;

            shop.Reset();

            Health = 25;
            Defence = 1;
            Damage = 4;
            Money = 0;
        });
        GameOver.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate
        {
            Application.Quit();
        });


        log = new Log(_logTextObject);
        player = _startPlayer;
        statistic_canvas = _startStatisticCanvas;
        statistics = new UI_System(statistic_canvas);
    }
    private void Update()
    {
    }
}