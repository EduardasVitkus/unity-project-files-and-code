using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System;

public class XML
{
    public static Tuple<Sprite[], List<int[,]>> LoadMap(string folderName)
    {
        string tileName = "T_";
        TextAsset XML_Text = Resources.Load<TextAsset>(folderName + "\\XML");
        XmlDocument xml = new XmlDocument();
        xml.LoadXml(XML_Text.text);
        int tilesWide = int.Parse(xml["tilemap"].GetAttribute("tileswide"));
        int tilesHigh = int.Parse(xml["tilemap"].GetAttribute("tileshigh"));
        int tileWidth = int.Parse(xml["tilemap"].GetAttribute("tilewidth"));
        int tileHeight = int.Parse(xml["tilemap"].GetAttribute("tileheight"));
        List<int[,]> layers = new List<int[,]>();
        int biggest = -1;
        foreach (XmlElement one in xml.FirstChild)
        {
            int[,] layer = new int[tilesWide, tilesHigh];
            foreach (XmlElement two in one)
            {
                int x = int.Parse(two.GetAttribute("x"));
                int y = int.Parse(two.GetAttribute("y"));
                int index = int.Parse(two.GetAttribute("tile"));
                layer[x, y] = index;
                biggest = index > biggest ? index : biggest;
            }
            layers.Add(layer);
        }
        Sprite[] sprites = new Sprite[biggest + 1];
        int precision = 1;
        while (!Resources.Load<Sprite>(folderName + "\\" + string.Format("T_{0," + precision + ":D" + precision + "}", 0)))
            precision++;
        string format = tileName + "{0," + precision + ":D" + precision + "}";
        for(int i = 0; i < biggest + 1; i++)
        {
            string name = string.Format(format, i);
            sprites[i] = Resources.Load<Sprite>(folderName + "\\" + name);
        }
        return new Tuple<Sprite[], List<int[,]>>(sprites, layers);
    }
}
