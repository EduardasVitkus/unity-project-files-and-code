using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void VOID_VOID();
public class Trigger_System
{
    Dictionary<string, VOID_VOID> dictionary;
    public Trigger_System()
    {
        dictionary = new Dictionary<string, VOID_VOID>();
    }
    private string Key(int x, int y)
    {
        return "" + x + "_" + y;
    }
    public void SetTrigger(int x, int y, VOID_VOID trigger)
    {
        dictionary[Key(x, y)] = trigger;
    }
    public void Invoke(int x, int y)
    {
        if(dictionary.ContainsKey(Key(x, y)))
        {
            VOID_VOID trigger = dictionary[Key(x, y)];
            trigger.Invoke();
        }
    }
    public void Reset()
    {
        dictionary = new Dictionary<string, VOID_VOID>();
    }
    public void RemoveTrigger(int x, int y)
    {
        dictionary[Key(x, y)] = null;
    }
}
