using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Log
{
    Text text;
    int current = 0;
    int max = 17;

    public Log(Text text)
    {
        this.text = text;
    }
    public void Clear()
    {
        text.text = "";
    }
    public void WriteLine(string data)
    {
        current = max;
        Convert(data);
    }
    public void Write(string data)
    {
        Convert(" " + data);
    }
    private void Convert(string data)
    {
        string temp = text.text;
        foreach (char one in data)
        {
            if (current == max)
            {
                current = 0;
                temp += '\n';
            }
            current++;
            temp += one;
        }
        text.text = temp;
    }
}
