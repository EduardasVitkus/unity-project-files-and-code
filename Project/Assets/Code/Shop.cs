using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Shop
{
    int[] prices = {10, 20, 30, 40, 50, 60, 70, 80, -1 };
    Dictionary<string, Tuple<Text, ProgressBar>> dictionary;
    public Shop(GameObject o)
    {
        dictionary = new Dictionary<string, Tuple<Text, ProgressBar>>();
        for(int i = 1; i < o.transform.childCount; i++)
        {
            Text value = o.transform.GetChild(i).GetChild(4).gameObject.GetComponent<Text>();
            value.text = prices[0].ToString();
            string key = o.transform.GetChild(i).GetChild(3).gameObject.GetComponent<Text>().text;
            Button btn = o.transform.GetChild(i).GetChild(1).gameObject.GetComponent<Button>();
            ProgressBar bar = new ProgressBar(o.transform.GetChild(i).GetChild(2).GetChild(1).gameObject);
            btn.onClick.AddListener(delegate 
            {
                if(bar.index + 1 == prices.Length)
                {
                    Core.log.WriteLine(string.Format("{0} maxed out", key));
                    return;
                }
                if (Core.Money >= prices[bar.index])
                {
                    switch(key)
                    {
                        case "Armour":
                            Core.Defence += 1;
                            break;
                        case "Damage":
                            Core.ShopUtilities.IncreaseDamage();
                            break;
                    }
                    Core.Money -= prices[bar.index];
                    bar.Increase();
                    value.text = prices[bar.index].ToString();
                    Core.log.WriteLine("Upgrade Bought");
                    if(prices[bar.index] == -1)
                    {
                        value.text = "MAX";
                        Core.log.WriteLine(string.Format("{0} maxed out", key));
                    }
                }
                else
                {
                    Core.log.WriteLine("Not enough money");
                }
            });
            dictionary.Add(key, new Tuple<Text, ProgressBar>(value, bar));
        }
    }
    public void Reset()
    {
        foreach(KeyValuePair<string, Tuple<Text, ProgressBar>> one in dictionary)
        {
            one.Value.Item2.Reset();
            one.Value.Item1.text = prices[0].ToString();
        }
    }
}
public class ProgressBar
{
    List<GameObject> bar;
    public int index = 0;
    public ProgressBar(GameObject o)
    {
        bar = new List<GameObject>();
        for(int i = 0; i < o.transform.childCount; i++)
        {
            GameObject single = o.transform.GetChild(i).gameObject;
            single.SetActive(false);
            bar.Add(single);
        }
    }
    public void Increase()
    {
        if (index < bar.Count)
        {
            bar[index].SetActive(true);
            index++;
        }
    }
    public void Reset()
    {
        index = 0;
        foreach(GameObject one in bar)
        {
            one.SetActive(false);
        }
    }
}
